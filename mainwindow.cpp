#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QTimer>

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

#include <QJsonDocument>
#include <QJsonObject>

//---------------------------------------
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_timer(nullptr),
    m_networkManager(nullptr)
{
    // ui setup
    ui->setupUi(this);
    this->setFixedSize(this->size());
    connect(this, SIGNAL(updatedBTCValue(QString)), ui->lblBTCValue, SLOT(setText(QString)));

    // timer setup
    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(onTimerTimeout()));

    // network setup
    m_networkManager = new QNetworkAccessManager(this);
    connect(m_networkManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(onNetworkResult(QNetworkReply*)));

    // make first request
    makeBTCValueRequest();

    // start timer
    m_timer->start(10000);
}

//----------------------------------------------------
void MainWindow::onNetworkResult(QNetworkReply* reply)
{
    if (reply->error() != QNetworkReply::NoError)
        // handle error
        return;

    QString strReply = (QString)reply->readAll();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
    QJsonObject jsonObj = jsonResponse.object();
    QString btcValue = jsonObj["last"].toString();

    emit updatedBTCValue(btcValue);

    delete reply;
}

//-------------------------------
void MainWindow::onTimerTimeout()
{
    makeBTCValueRequest();
}

//------------------------------------
void MainWindow::makeBTCValueRequest()
{
    QUrl url("https://www.bitstamp.net/api/v2/ticker/btceur/");
    QNetworkRequest request;
    request.setUrl(url);
    m_networkManager->get(request); // GET
}

//-----------------------
MainWindow::~MainWindow()
{
    delete ui;
    delete m_timer;
    delete m_networkManager;
}
