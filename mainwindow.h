#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QNetworkAccessManager;
class QNetworkReply;
class QTimer;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

signals:
    void updatedBTCValue(QString value);

public slots:
    void onTimerTimeout();
    void onNetworkResult(QNetworkReply*);

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    void makeBTCValueRequest();

private:
    Ui::MainWindow *ui;

    QTimer* m_timer;

    QNetworkAccessManager* m_networkManager;
};

#endif // MAINWINDOW_H
